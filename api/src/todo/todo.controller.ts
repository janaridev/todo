import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { TodoService } from './todo.service';
import { CreateToDoDto } from './dtos/createToDo.dto';
import { Request, Response } from 'express';
import { UpdateToDoDto } from './dtos/updateToDo.dto';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from 'src/user/guards/authGuard';
import { UserService } from 'src/user/user.service';

@ApiTags('todo')
@UseGuards(AuthGuard)
@Controller('/api')
export class TodoController {
  constructor(
    private readonly toDoService: TodoService,
    private readonly userService: UserService,
  ) {}

  @Get('/todos')
  @ApiResponse({ status: 200, description: 'Fetched all todos successfully.' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 404, description: 'User was not found' })
  @ApiResponse({
    status: 500,
    description: 'Something went wrong while fetching todos.',
  })
  async getToDosHandler(@Req() req: Request, @Res() res: Response) {
    try {
      const token = this.toDoService.extractTokenFromHeader(req);
      const userId = this.toDoService.getIdFromToken(token);

      const user = await this.userService.findUserById(userId);
      if (!user) {
        res.status(404).send({ message: 'User was not found.' });
      }

      const todos = await this.toDoService.getToDos(userId);
      return res.status(200).send(todos);
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: 'Something went wrong.' });
    }
  }

  @Get('todo/:id')
  @ApiResponse({ status: 200, description: 'Fetched todo successfully.' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({
    status: 404,
    description: 'Todo was not found or User was not found.',
  })
  @ApiResponse({
    status: 500,
    description: 'Something went wrong while fetching todo.',
  })
  async getToDoByIdHandler(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const token = this.toDoService.extractTokenFromHeader(req);
      const userId = this.toDoService.getIdFromToken(token);

      const user = await this.userService.findUserById(userId);
      if (!user) {
        res.status(404).send({ message: 'User was not found.' });
      }

      const todo = await this.toDoService.getToDoById(id, userId);
      if (todo === null) {
        return res.status(404).send({ message: 'To Do was not found.' });
      }

      return res.status(200).send(todo);
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: 'Something went wrong.' });
    }
  }

  @Post('/todo')
  @ApiResponse({ status: 201, description: 'Todo was created successfully.' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 400, description: 'Some fields are not filled.' })
  @ApiResponse({ status: 404, description: 'User was not found' })
  @ApiResponse({
    status: 500,
    description: 'Something went wrong while creating todo.',
  })
  async createToDoHandler(
    @Body() createToDo: CreateToDoDto,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const token = this.toDoService.extractTokenFromHeader(req);
      const userId = this.toDoService.getIdFromToken(token);

      const user = await this.userService.findUserById(userId);
      if (!user) {
        res.status(404).send({ message: 'User was not found.' });
      }

      const createdToDo = await this.toDoService.createToDo(createToDo, userId);

      return res.status(201).send(createdToDo);
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: 'Something went wrong.' });
    }
  }

  @Put('todo/:id')
  @ApiResponse({ status: 201, description: 'Todo was updated successfully.' })
  @ApiResponse({ status: 400, description: 'Some fields are not filled.' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({
    status: 404,
    description: 'Todo was not found or User was not found.',
  })
  @ApiResponse({
    status: 500,
    description: 'Something went wrong while updating todo.',
  })
  async updateToDoHandler(
    @Param('id') id: string,
    @Body() updateToDo: UpdateToDoDto,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const token = this.toDoService.extractTokenFromHeader(req);
      const userId = this.toDoService.getIdFromToken(token);

      const user = await this.userService.findUserById(userId);
      if (!user) {
        res.status(404).send({ message: 'User was not found.' });
      }

      const todo = await this.toDoService.getToDoById(id, userId);
      if (todo === null) {
        return res.status(404).send({ message: 'To Do was not found.' });
      }

      const updatedToDo = await this.toDoService.updateToDo(todo, updateToDo);

      return res.status(200).send(updatedToDo);
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: 'Something went wrong.' });
    }
  }

  @Delete('/todo/:id')
  @ApiResponse({ status: 201, description: 'Todo was deleted successfully.' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 404, description: 'Todo was not found.' })
  @ApiResponse({
    status: 500,
    description: 'Something went wrong while deleting todo.',
  })
  async deleteToDoHandler(
    @Param('id') id: string,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const token = this.toDoService.extractTokenFromHeader(req);
      const userId = this.toDoService.getIdFromToken(token);

      const user = await this.userService.findUserById(userId);
      if (!user) {
        res.status(404).send({ message: 'User was not found.' });
      }

      const todo = await this.toDoService.getToDoById(id, userId);
      if (todo === null) {
        return res.status(404).send({ message: 'To Do was not found.' });
      }

      await this.toDoService.deleteToDo(todo);
      return res.status(200).send({ message: 'To Do was deleted.' });
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: 'Something went wrong.' });
    }
  }
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ToDo } from 'src/entities/todo.entity';
import { Repository } from 'typeorm';
import { CreateToDoDto } from './dtos/createToDo.dto';
import { UpdateToDoDto } from './dtos/updateToDo.dto';
import { Request } from 'express';
import jwtDecode from 'jwt-decode';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(ToDo) private readonly todoRepo: Repository<ToDo>,
  ) {}

  async getToDos(userId: string) {
    const todos = await this.todoRepo.find({
      where: {
        userId,
      },
    });
    return todos;
  }

  async getToDoById(id: string, userId: string) {
    const todo = await this.todoRepo.findOne({
      where: {
        id,
        userId,
      },
    });
    return todo;
  }

  async createToDo(createToDo: CreateToDoDto, userId: string) {
    const todo = this.todoRepo.create(createToDo);
    todo.createdAt = new Date();
    todo.userId = userId;

    await this.todoRepo.save(todo);

    return todo;
  }

  async updateToDo(todo: ToDo, updateToDo: UpdateToDoDto) {
    Object.assign(todo, updateToDo);

    return await this.todoRepo.save(todo);
  }

  async deleteToDo(todo: ToDo) {
    await this.todoRepo.remove(todo);
  }

  extractTokenFromHeader(request: Request): string {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return token;
  }

  getIdFromToken(token: string) {
    const decodedToken = jwtDecode(token);
    return decodedToken['sub'];
  }
}

import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsString, MaxLength, MinLength } from 'class-validator';

class ToDoForManipulationDto {
  @ApiProperty()
  @IsString()
  @MinLength(4)
  @MaxLength(12)
  public title: string;

  @ApiProperty()
  @IsString()
  @MinLength(4)
  @MaxLength(12)
  public description: string;

  @ApiProperty()
  @IsDateString()
  public deadLine: Date;
}

export default ToDoForManipulationDto;

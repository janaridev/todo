import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength, MinLength } from 'class-validator';

class UserDto {
  @ApiProperty()
  @IsString()
  @MinLength(4)
  @MaxLength(10)
  username: string;

  @ApiProperty()
  @IsString()
  @MinLength(6)
  @MaxLength(20)
  password: string;
}

export default UserDto;

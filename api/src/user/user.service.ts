import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/entities/user.entity';
import { Repository } from 'typeorm';
import UserDto from './dtos/user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
  ) {}

  async findUser(username: string) {
    const user = await this.userRepo.findOne({
      where: {
        username,
      },
    });
    return user;
  }

  async findUserById(userId: string) {
    const user = await this.userRepo.findOne({
      where: {
        id: userId,
      },
    });
    return user;
  }

  async createUser(userDto: UserDto) {
    const user = this.userRepo.create(userDto);
    await this.userRepo.save(user);

    return {
      id: user.id,
      username: user.username,
    };
  }
}

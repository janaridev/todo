import { Body, Controller, Post, Res } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { UserService } from './user.service';
import UserDto from './dtos/user.dto';
import { Response } from 'express';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

@ApiTags('user')
@Controller('/api/user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  @Post('/register')
  @ApiResponse({ status: 201, description: 'User was created successfully.' })
  @ApiResponse({ status: 400, description: 'Some fields are not filled.' })
  @ApiResponse({ status: 404, description: 'User was not found.' })
  @ApiResponse({
    status: 500,
    description: 'Something went wrong while creating user.',
  })
  async registerHandler(@Body() userDto: UserDto, @Res() res: Response) {
    try {
      const user = await this.userService.findUser(userDto.username);
      if (user) {
        return res.status(404).send({
          message: `User with username: ${userDto.username} already exists`,
        });
      }

      const createdUser = await this.userService.createUser(userDto);
      return res.status(201).send(createdUser);
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: 'Something went wrong.' });
    }
  }

  @Post('/login')
  @ApiResponse({ status: 200, description: 'User was logged in successfully.' })
  @ApiResponse({
    status: 400,
    description: 'Some fields are not filled or password is not valid.',
  })
  @ApiResponse({ status: 404, description: 'User was not found.' })
  @ApiResponse({
    status: 500,
    description: 'Something went wrong while creating user.',
  })
  async loginHandler(@Body() userDto: UserDto, @Res() res: Response) {
    try {
      const user = await this.userService.findUser(userDto.username);
      if (!user) {
        return res.status(404).send({
          message: `User with username: ${userDto.username} was not found.`,
        });
      }

      const validPassword = await bcrypt.compare(
        userDto.password,
        user.password,
      );

      if (!validPassword) {
        return res.status(400).send({ message: 'Password is not valid.' });
      }

      const payload = {
        sub: user.id,
        username: user.username,
      };

      return res
        .status(200)
        .send({ access_token: await this.jwtService.signAsync(payload) });
    } catch (e) {
      console.error(e);
      res.status(500).send({ message: 'Something went wrong.' });
    }
  }
}
